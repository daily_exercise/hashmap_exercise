import java.util.HashMap;

public class two {

    /**
     * Write a Java program to count the number of key-value (size) mappings in a
     * map.
     */
    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        System.out.println(hm.size());
    }
}
