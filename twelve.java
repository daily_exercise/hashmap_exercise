import java.util.HashMap;

public class twelve {

    /**
     * Write a Java program to get a collection view of the values contained in this
     * map
     */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        hm.put(7, "seven");
        hm.put(9, "nine");
        System.out.println(hm.values());
    }

}
