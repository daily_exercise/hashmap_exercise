import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Iterator;

public class one {

    /**
     * Write a Java program to associate the specified value with the specified key
     * in a HashMap.
     * 
     * @param args
     */
    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");

        for (Entry<Integer, String> it : hm.entrySet()) {
            System.out.println(it.getKey() + " " + it.getValue());
        }

    }
}