import java.util.HashMap;

public class ten {

    /** Write a Java program to get the value of a specified key in a map. */
    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        hm.put(7, "seven");
        hm.put(9, "nine");

        String value_of_key = hm.get(4);
        System.out.println(value_of_key);

    }

}
