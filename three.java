import java.util.HashMap;

public class three {
    /**
     * Write a Java program to copy all of the mappings from the specified map to
     * another map.
     */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");

        HashMap<Integer, String> hm2 = new HashMap<>();
        hm2.putAll(hm);

    }

}
