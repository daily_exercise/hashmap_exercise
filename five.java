import java.util.HashMap;

public class five {
    /**
     * Write a Java program to check whether a map contains key-value mappings
     * (empty) or not.
     */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");

        boolean anyPair = hm.isEmpty();
    }

}
