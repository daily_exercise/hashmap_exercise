import java.util.HashMap;

public class eleven {
    /** Write a Java program to get a set view of the keys contained in this map. */
    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        hm.put(7, "seven");
        hm.put(9, "nine");
        System.out.println(hm.keySet());
    }

}
