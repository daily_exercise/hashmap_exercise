import java.util.HashMap;

public class six {

    /** Write a Java program to get a shallow copy of a HashMap instance. */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");

        // Shallow copy
        hm.clone();
    }

}
