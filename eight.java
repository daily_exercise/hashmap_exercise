import java.util.HashMap;

public class eight {
    /**
     * Write a Java program to test if a map contains a mapping for the specified
     * value.
     */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        hm.containsValue("four");
    }
}
