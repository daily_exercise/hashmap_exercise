import java.util.HashMap;
import java.util.Set;

public class nine {

    /**
     * Write a Java program to create a set view of the mappings contained in a map.
     */

    public static void main(String[] args) {
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(2, "two");
        hm.put(4, "four");
        hm.put(7, "seven");
        hm.put(9, "nine");
        Set it = hm.entrySet();
        System.out.println("Set view : " + it);
    }

}
